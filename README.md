**Delocal test task (API)**

**_Description_**:

Create a REST-like API without DELETE functionality

The API represents a contacts entity and reachable through http. The contacts should be stored in a database and the input/output of the API should be in JSON​ format:

- return all contacts: http://example.com/contacts
- return a contact by id: http://example.com/contacts/{id}
- modify a contact (change email at an existing contact...)
- create a contact (all fields are required)
Reading should be handled with GET​ and writing with PUT​.

Data structure of a contact:

- name
- email
- phone number
- address
Additional info:
You should use Php.
You should not use any framework.
You should write production code that you would push to a repository and solves the feature.
You should submit the solution even if it is not fully finished.

**_Requirements_**:

- Apache 2.2/2.4 with PHP 5.6 installed
- Mysql server 5+

**_Install_**:

- Clone the repository
- Create the schema in an accessible MySQL database server
- Run the Data/schema.sql file to create the necessary "contact" table
- Set the database access info in Lib/Config/config.yml:
`database:
  user: username
  pass: password
  schema: database_name
  host: database_host`
- Get the needed dependecies with Composer:
    Run `composer install` in project root (PHP 5.6) needed.

**_Contact_**:
    hemuveg@gmail.com
