<?php
namespace Entity;

use Repository\ContactRepository;

class Contact
{
  /**
   * @var integer
   */
  private $id;

  /**
   * @var string
   */
  private $name;

  /**
   * @var string
   */
  private $email;

  /**
   * @var string
   */
  private $phone_number;

  /**
   * @var string
   */
  private $address;

  /**
   * @var ContactRepository
   */
  private static $repo;

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   * @return Contact
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param string $name
   * @return Contact
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @param string $email
   * @return Contact
   */
  public function setEmail($email)
  {
    $this->email = $email;
    return $this;
  }

  /**
   * @return string
   */
  public function getPhone_number()
  {
    return $this->phone_number;
  }

  /**
   * @param string $phone_number
   * @return Contact
   */
  public function setPhone_number($phone_number)
  {
    $this->phone_number = $phone_number;
    return $this;
  }

  /**
   * @return string
   */
  public function getAddress()
  {
    return $this->address;
  }

  /**
   * @param string $address
   * @return Contact
   */
  public function setAddress($address)
  {
    $this->address = $address;
    return $this;
  }

  public static function getRepo()
  {
    if ( !self::$repo )
    {
      self::$repo = new ContactRepository();
    }

    return self::$repo;
  }

  public function toArray()
  {
    return array(
      'id'           => $this->getId(),
      'name'         => $this->getName(),
      'address'      => $this->getAddress(),
      'email'        => $this->getEmail(),
      'phone_number' => $this->getPhone_number(),
    );
  }
}
