<?php
namespace Helper;

use Config\Config;

abstract class BaseAction
{
  /**
   * @var Request
   */
  private $request;

  /**
   * BaseAction constructor.
   * @param Request $request
   * @throws \Exception
   */
  public function __construct(Request $request)
  {
    // get config
    $conf = Config::getInstance()->get('request');
    if (!isset($conf['allowed_methods'])) {
      throw new \Exception('Config file error!', 400);
    }
    if (!in_array($request->getMethod(), $conf['allowed_methods'])) {
      throw new \Exception('HTTP method not allowed!', 400);
    }
    $this->request = $request;
    $this->handleRequest();
  }

  /**
   * Gets the current HTTP method
   * @return string
   */
  private function getMethod()
  {
    return $this->request->getMethod();
  }

  /**
   * Handles the request
   * Gets the function name to call
   * Calls the function
   */
  private function handleRequest()
  {
    // get the function to call from URI
    $call = $this->getActionToCall();
    // call the action with the params from URI
    call_user_func_array(array($this, $call['action']), $call['params']);
  }

  /**
   * Simplified "mini-router"
   * @return array
   * @throws \Exception
   */
  private function getActionToCall()
  {
    $routes = $this->getRoutes();
    if ( !$routes )
    {
      throw new \Exception('Routes are not defined', 400);
    }
    if ( !isset($routes[$this->getMethod()]) )
    {
      throw new \Exception('Routes not found', 400);
    }

    $call_action = null;
    $call_params = array();
    $uri = $this->request->getUri();
    foreach ( $routes[$this->getMethod()] as $action => $regexp )
    {
      if ( preg_match('~^' . $regexp . '$~', $uri, $m) )
      {
        $call_action = $action;
        if ( count($m) > 1 )
        {
          array_shift($m);
          $call_params = $m;
        }
        break;
      }
    }
    if ( !$call_action )
    {
      throw new \Exception('Route not found', 400);
    }

    if ( !is_callable(array($this, $call_action)) )
    {
      throw new \Exception('Action not found', 400);
    }

    return array(
      'action' => $call_action,
      'params' => $call_params,
    );
  }

  /**
   * Returns json formatted response
   * @param $array
   */
  public function renderJson($array, $response_code = 200)
  {
    return $this->render(json_encode($array), 'application/json', $response_code);
  }

  /**
   * Returns the given error message with a http code 400/given
   * @param mixed $content
   * @param int $error_code
   */
  public function renderError($content, $format = 'text/html', $error_code = 400)
  {
    return $this->render($content, $format, $error_code);
  }

  /**
   * Returns a response with given content and format
   * @param $content
   * @param string $format
   */
  public function render($content, $format = 'text/html', $response_code = 200)
  {
    http_response_code($response_code);
    header('Content-type: ' . $format);
    echo $content;
  }

  /**
   * @return Request
   */
  public function getRequest()
  {
    return $this->request;
  }

  /**
   * Return an array of possible routes
   * Could be extended to use config.yml for "route to action" transformations
   * @return array|null
   */
  abstract function getRoutes();
}
