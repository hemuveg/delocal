<?php
namespace Helper;

class Request
{
  /**
   * @var Request
   */
  private static $instance;

  /**
   * @var string
   */
  private $method;

  /**
   * @var mixed
   */
  private $content;

  /**
   * @var boolean
   */
  private $authenticated;

  /**
   * Request constructor.
   */
  public function __construct()
  {
    $method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
    $this->setMethod($method);

    // process authentication data
    $this->processAuth();

    // process request's body if any
    if ( $method == 'PUT' )
    {
      $content = file_get_contents('php://input');
      if ( $content )
      {
        $this->setContent($content);
      }
    }
  }

  /**
   * @return Request
   */
  public static function getInstance()
  {
    if ( !self::$instance )
    {
      self::$instance = new self;
    }

    return self::$instance;
  }

  /**
   * @return string
   */
  public function getMethod()
  {
    return $this->method;
  }

  /**
   * @param string $method
   */
  public function setMethod($method)
  {
    $this->method = $method;
  }

  /**
   * @return mixed
   */
  public function getContent()
  {
    return $this->content;
  }

  /**
   * @param mixed $content
   */
  public function setContent($content)
  {
    $this->content = $content;
  }

  /**
   * Checks for authentication data in request
   * @param bool $default
   */
  public function processAuth($default = false)
  {
    // for now it is always true, but we can implement some header checking function
    $this->authenticated = true;
  }

  /**
   * @return boolean
   */
  public function IsAuthenticated()
  {
    return $this->isAuthenticated;
  }

  /**
   * Returns the current URI
   * @return string
   */
  public function getUri()
  {
    $root_uri = dirname($_SERVER['SCRIPT_NAME']);
    $uri = rtrim(str_replace($root_uri, '', $_SERVER['REQUEST_URI']), '/');

    return $uri;
  }
}
