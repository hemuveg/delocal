<?php
namespace Config;

use Symfony\Component\Yaml\Yaml;

class Config
{
  private static $instance;

  /**
   * @var mixed
   */
  private $data;

  public function __construct()
  {
    $root_path = dirname($_SERVER['SCRIPT_FILENAME']);
    $config_path = $root_path . '/Lib/Config/config.yml';
    if ( !is_file($config_path) )
    {
      throw new \Exception('Config file not found!', 400);
    }

    $this->data = Yaml::parse(file_get_contents($config_path));
  }

  public function get($key)
  {
    if ( !isset($this->data[$key]) )
    {
      return array();
    }

    return $this->data[$key];
  }

  public static function getInstance()
  {
    if ( !self::$instance )
    {
      self::$instance = new self();
    }

    return self::$instance;
  }
}