<?php
namespace Action;

use Config\Config;
use Helper\BaseAction;
use Entity\Contact;

class RestAction extends BaseAction
{
  public function getRoutes()
  {
    return array(
      'GET' => array(
        'listAction' => '/contacts',
        'viewAction' => '/contacts/([0-9]+)'
      ),
      'PUT' => array(
        'createAction' => '/contacts',
        'updateAction' => '/contacts/([0-9]+)',
      ),
    );
  }

  public function listAction()
  {
    $items = Contact::getRepo()->findAll();
    if ( !count($items) )
    {
      return $this->renderError('No items found');
    }

    $res = array();
    foreach ( $items as $item )
    {
      $res[] = $item->toArray();
    }

    return $this->renderJson($res);
  }

  public function viewAction($id)
  {
    if ( !$id )
    {
      return $this->renderError('Id required!');
    }
    $item = Contact::getRepo()->find($id);
    if ( !$item )
    {
      return $this->renderError('Contact does not exist!');
    }

    return $this->renderJson($item->toArray());
  }

  public function createAction()
  {
    $fields = json_decode($this->getRequest()->getContent(), true);
    $contact = new Contact();
    foreach ( $fields as $field_name => $value )
    {
      $func_name = 'set' . ucfirst($field_name);
      call_user_func_array(array($contact, $func_name), array($value));
    }
    $repo = Contact::getRepo();
    $res = $repo->save($contact);
    if ( !$res )
    {
      return $this->renderError('Error!' . $repo->getLastError());
    }

    return $this->render('ok');
  }

  public function updateAction($id)
  {
    $conf = Config::getInstance()->get('Contact');
    $fields = json_decode($this->getRequest()->getContent(), true);
    $diff = array_diff(array_keys($fields), $conf['update_allowed']);
    if ( count($diff) )
    {
      return $this->renderError('Field(s): "' . implode(', ', $diff) . '" are not allowed to update!');
    }
    if ( !$id )
    {
      return $this->renderError('Id required!');
    }
    $repo = Contact::getRepo();
    /* @var $item Contact */
    $item = $repo->find($id);
    if ( !$item )
    {
      return $this->renderError('Contact does not exist!');
    }

    $item->setEmail($fields['email']);
    if ( !$this->validateContact($item) )
    {
      return $this->renderError('Contact is not valid: ' . print_r($item->toArray(), true));
    }
    $res = $repo->save($item);

    if ( !$res )
    {
      return $this->renderError('Error!' . $repo->getLastError());
    }

    return $this->render('ok');
  }

  private function validateContact(Contact $contact)
  {
    //TODO: get config for Contact, check field validity e.g. email with regex
    // for now return true
    return true;
  }
}
