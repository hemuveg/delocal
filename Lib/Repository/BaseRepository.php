<?php
namespace Repository;

use Config\Config;

abstract class BaseRepository
{
  /**
   * @var PDO
   */
  private $conn;

  /**
   * @var string
   */
  private $lastError;

  /**
   * BaseRepository constructor.
   */
  public function __construct()
  {
    $config = Config::getInstance()->get('database');
    $this->conn = new \PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['schema'] . ';charset=UTF8', $config['user'], $config['pass']);
  }

  /**
   * Find an entity by the given id
   * @param $id
   * @return mixed
   */
  public function find($id)
  {
    $className = $this->getClassName();
    $q = '
    SELECT
      *
    FROM
      ' . $this->getTableName() . '
    WHERE
      id = :id
    ';
    $stmt = $this->conn->prepare($q);
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    $stmt->execute();
    $stmt->setFetchMode(\PDO::FETCH_CLASS, $className);

    return $stmt->fetch();
  }

  /**
   * Find all entities in the given table
   * @return array
   */
  public function findAll()
  {
    $className = $this->getClassName();
    $q = '
    SELECT
      *
    FROM
      ' . $this->getTableName();
    $stmt = $this->conn->prepare($q);
    $stmt->execute();
    $stmt->setFetchMode(\PDO::FETCH_CLASS, $className);

    return $stmt->fetchAll();
  }

  /**
   * Saves the entity and return the status
   * @param $item
   * @return bool
   */
  public function save($item)
  {
    $class = $this->getClassName();
    if ( !$item instanceof $class )
    {
      $this->lastError = 'Class not supported!';
      return false;
    }
    $data = $item->toArray();
    // save new
    if ( !$item->getId() )
    {
      unset($data['id']);
      $q = 'INSERT INTO ' . $this->getTableName() . ' (' . implode(', ', array_keys($data)) . ') VALUES (:' . implode(',:', array_keys($data)) . ')';
      $stmt = $this->conn->prepare($q);

      return $stmt->execute($data);
    }
    else
    {
      $keys = array_keys($data);
      $fields = array();
      foreach ($keys as $key )
      {
        if ( $key == 'id' )
        {
          continue;
        }
        $fields[] = $key . ' = :' . $key;
      }
      $q = 'UPDATE ' . $this->getTableName() . ' SET ' . implode(', ', $fields) . ' WHERE id = :id';
      $stmt = $this->conn->prepare($q);

      return $stmt->execute($data);
    }
  }

  /**
   * @return string
   */
  public function getLastError()
  {
    return $this->lastError;
  }

  /**
   * Returns the name of the class the repository handles
   * @return string
   */
  abstract function getClassName();

  /**
   * Returns the database table name for the given class
   * @return string
   */
  abstract function getTableName();
}