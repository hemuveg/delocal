<?php
namespace Repository;

use Entity\Contact;

class ContactRepository extends BaseRepository
{
  /**
   * {inheritdoc}
   */
  public function getClassName()
  {
    return Contact::class;
  }

  /**
   * {inheritdoc}
   */
  public function getTableName()
  {
    return 'contact';
  }
}
