<?php
require_once __DIR__ . '/vendor/autoload.php';

use Helper\Request;
use Action\RestAction;

$request = Request::getInstance();

try
{
  $handler = new RestAction($request);
}
catch (Exception $e)
{
  header('Application error', true, $e->getCode());
  echo $e->getMessage();
  exit;
}
